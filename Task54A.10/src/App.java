import model.Circle;

public class App {
    public static void main(String[] args) throws Exception {
        // System.out.println("Hello, World!");
        Circle circle1 = new Circle();
        // circle1.setRadius(2.0);
        Circle circle2 = new Circle(3.0);
        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        System.out.println("Chu vi hinh tron 1:");
        System.out.println(circle1.getCircumference());
        System.out.println("Dien tich hinh tron 1:");
        System.out.println(circle1.getArea());
        System.out.println("Chu vi hinh tron 2:");
        System.out.println(circle2.getCircumference());
        System.out.println("Dien tich hinh tron 2:");
        System.out.println(circle2.getArea());
    }
}
