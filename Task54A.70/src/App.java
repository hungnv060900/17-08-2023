import model.Time;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        Time time1 = new Time(12, 30, 45);
        Time time2 = new Time(23, 45, 15);

        System.out.println("Time 1: " + time1);
        System.out.println("Time 2: " + time2);

        time1.nextSecond();
        time2.previousSecond();

        System.out.println("Time 1 after nextSecond: " + time1);
        System.out.println("Time 2 after previousSecond: " + time2);
    }
}
