import model.Date;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        Date date1 = new Date(11, 9, 2021);
        Date date2 = new Date(6, 9, 2000);
        System.out.println(date1.toString());
        System.out.println(date2.toString());
    }
}
