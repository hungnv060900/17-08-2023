import model.Employee;

public class App {
    public static void main(String[] args) throws Exception {
        // System.out.println("Hello, World!");
        Employee employee1 = new Employee(1, "Viet", "Hung", 125000);
        Employee employee2 = new Employee(2, "Huy", "Toan", 800000);
        System.out.println(employee1);
        System.out.println(employee2);
        System.out.println("Ten day du la:");
        System.out.println(employee1.getName());
        System.out.println(employee2.getName());
        System.out.println("Luong 1 nam cua nhan vien la:");
        System.out.println(employee1.getAnnualSalary());
        System.out.println(employee2.getAnnualSalary());
        System.out.println("Tang luong cho nhan vien:");
        System.out.println(employee1.raiseSalary(10));
        System.out.println(employee2.raiseSalary(20));

    }
}
