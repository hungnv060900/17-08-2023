import model.InvoiceItem;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        InvoiceItem invoiceItem1 = new InvoiceItem("1", "Hoa don dien nuoc",3 , 10000);
        InvoiceItem invoiceItem2 = new InvoiceItem("2", "Hoa don tien nha",2 , 150000);
        System.out.println(invoiceItem1.toString());
        System.out.println(invoiceItem2.toString());
        System.out.println("Tong gia:");
        System.out.println("Tong gia don hang 1:");
        System.out.println(invoiceItem1.getTotal());
        System.out.println("Tong gia don hang 2:");
        System.out.println(invoiceItem2.getTotal());

    }
}
